#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;

/*
    A function to create masks for extracting specific bits from a number
*/
unsigned extractBits(unsigned a, unsigned b)
{
   unsigned r = 0;
   for (unsigned i = a; i <= b; ++i)
       r |= 1 << i;
   return r;
}

/*
    A function to print a number in base-2
*/
void binary(unsigned n)
{
    if ((n >> 1) != 0) {
        binary((n >> 1));
    }
    printf("%d", n & 1);
}

/*
    A function to swap bits and right bits of a number.
    Note: If there are odd number of bits the middle bit remains at its place
        and bits on both of its sides are swapped.
*/
unsigned swapNum(unsigned n)
{
    if (n >> 1 == 0)
        return n;
    unsigned leftMask, rightMask, left, right, temp, returnValue = 0;
    int bits = 0;

    //calculating the number of bits in 'n'
    temp = n;
    while (1)
    {
        ++bits;
        temp >>= 1;
        if (temp == 0)
            break;
    }
	
    rightMask = extractBits(0, (bits >> 1) - 1);
    right = n & rightMask;
    returnValue = right;

    if (bits & 1) // if odd number of bits
    {
        short middleBit = 1;
        temp = n >> (bits >> 1);
        middleBit &= temp;
        left = temp >> 1;

        returnValue <<= 1;
        returnValue |= middleBit;
    }
    else // if even number of bits
    {
        leftMask = extractBits((bits >> 1), bits - 1);
        left = n & leftMask;
        left >>= (bits >> 1);
    }

    returnValue <<= (bits >> 1);
    returnValue |= left;
    return returnValue;
}

int main()
{
    unsigned arr[] = {0, 1, 2, 3, 4, 5, 23, 36, 45, 64, 65, 73, 89, 100, 156, 260, 956, 2000, 2577338305, 2147483648, 2147483649}, output;
    vector <unsigned> inp (arr, arr + sizeof arr / sizeof arr[0]);
    for (vector<unsigned>::iterator i = inp.begin(); i != inp.end(); ++i)
    {
        cout << "Input: " << *i << ", binary: ";
        binary(*i);
        output = swapNum(*i);
        cout << "\nOutput: " << output << ", binary: ";
        binary(output);
	cout<<endl<<endl;
    }
    return 0;
}

